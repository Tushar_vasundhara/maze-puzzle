﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.IO;
using UnityEditor.SceneManagement;
using System.Collections.Generic;

public class LevelEditor : EditorWindow
{
    private static LevelEditor window;
    private int maxRows = 11;
    private int maxCols = 11;
    public static int[,] levelSquares = new int[80, 11];
    private Texture[] ballTex;
    int levelNumber = 1;
    private Vector2 scrollViewVector;
    private int limit;
    private int star1;
    private int star2;
    private int star3;
    private string fileName = "1.txt";
    private int brush;
    private float CameraX;
    private float CameraY;
    private float CameraZ;
    private float ProjectionSize;
    LevelEditerAssets lA;
    private static int selectedTab;
    string[] toolbarStrings = new string[] { "Editor", "Settings", "In-apps", "Ads", "Help" };
    private bool enableGoogleAdsProcessing;
    const string itemEditorPath = "Assets/RaccoonRescue/Resources/ItemsEditor.asset";
    int[] powerups = new int[4];
    

    [MenuItem("Window/Game Editor")]
    public static void Init()
    {
        // Get existing open window or if none, make a new one:
        window = (LevelEditor)EditorWindow.GetWindow(typeof(LevelEditor));
        window.Show();

    }

    public static void ShowHelp()
    {
        selectedTab = 3;
    }

    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(LevelEditor));

    }

    void OnFocus()
    {
        Initialize();
    }

    void Initialize()
    {
        //GameObject gm = Resources.Load("LevelEditorBase") as GameObject;
        GameObject gm = AssetDatabase.LoadAssetAtPath("Assets/Resources/Prefabs/LevelEditorBase.prefab",
                            typeof(GameObject)) as GameObject;
        lA = gm.GetComponent<LevelEditerAssets>();

        levelSquares = new int[70, 11];
        for (int i = 0; i < maxRows; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                levelSquares[i, j] = 0;
            }
        }

    }

    void OnGUI()
    {
        GUI.changed = false;
        //LoadDataFromLocal(1);

        if (levelNumber < 1)
            levelNumber = 1;

        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Space(30);
        int oldSelected = selectedTab;
        selectedTab = GUILayout.Toolbar(selectedTab, toolbarStrings, new GUILayoutOption[] { GUILayout.Width(350) });
        GUILayout.EndHorizontal();


        scrollViewVector = GUI.BeginScrollView(new Rect(25, 45, position.width - 30, position.height), scrollViewVector, new Rect(0, 0, 400, 4200));
        GUILayout.Space(-30);


        if (selectedTab == 0)
        {

            GUILevelSelector();
            GUILayout.Space(10);

            //GUILimit();
            //GUILayout.Space(10);

            //GUIStars();
            //GUILayout.Space(10);

            GUICameraPosition();
            GUILayout.Space(10);

            GUIBlocks();
            GUILayout.Space(20);

            GUIGameField();

        }
        //else if (selectedTab == 1)
        //{
        //    GUISettings();


        //}
        //else if (selectedTab == 2)
        //{
        //    GUIInappSettings();

        //}
        //else if (selectedTab == 3)
        //{
        //    GUIAds();

        //}
        //else if (selectedTab == 4)
        //{
        //    GUIHelp();
        //}

        //if (GUI.changed)
        //{
        //    if (!EditorApplication.isPlaying)
        //        EditorSceneManager.MarkAllScenesDirty();
        //    //          EditorUtility.SetDirty (ItemsEditor.Instance);
        //}


        //if (enableGoogleAdsProcessing)
        //RunOnceGoogle();

        GUI.EndScrollView();
    }
    void GUILevelSelector()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Level editor", EditorStyles.boldLabel, new GUILayoutOption[] { GUILayout.Width(70) });

        GUILayout.EndHorizontal();

        //     myString = EditorGUILayout.TextField("Text Field", myString);
        GUILayout.BeginHorizontal();
        GUILayout.Space(30);
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Level:", EditorStyles.boldLabel, new GUILayoutOption[] { GUILayout.Width(50) });
        if (GUILayout.Button("<<", new GUILayoutOption[] { GUILayout.Width(50) }))
        {
            PreviousLevel();

        }
        string changeLvl = GUILayout.TextField(levelNumber.ToString(), new GUILayoutOption[] { GUILayout.Width(50) });
        levelNumber = int.Parse(changeLvl);

        if (GUILayout.Button(">>", new GUILayoutOption[] { GUILayout.Width(50) }))
        {
            NextLevel();
        }

        if (GUILayout.Button("Save level", new GUILayoutOption[] { GUILayout.Width(100) }))
        {
            SaveLevel();
        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Space(60);
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

    }

    void NextLevel()
    {
        levelNumber++;
        LoadDataFromLocal(levelNumber);
    }

    void PreviousLevel()
    {
        levelNumber--;
        if (levelNumber < 1)
            levelNumber = 1;
        LoadDataFromLocal(levelNumber);
    }
    public bool LoadDataFromLocal(int currentLevel)
    {
        //Read data from text file
        TextAsset mapText = Resources.Load("Levels/" + currentLevel) as TextAsset;
        if (mapText == null)
        {
            return false;
            mapText = Resources.Load("Levels/" + currentLevel) as TextAsset;
        }
        ProcessGameDataFromString(mapText.text);
        return true;
    }

    void ProcessGameDataFromString(string mapText)
    {
        string[] lines = mapText.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
        int mapLine = 0;
        foreach (string line in lines)
        {
            //if (line.StartsWith("SIZE "))
            //{
            //    string blocksString = line.Replace("SIZE ", string.Empty).Trim();
            //    string[] sizes = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            //    maxCols = int.Parse(sizes[0]);
            //    maxRows = int.Parse(sizes[1]);
            //    Initialize();
            //}
            //else if (line.StartsWith("LIMIT "))
            //{
            //    string blocksString = line.Replace("LIMIT ", string.Empty).Trim();
            //    //string[] sizes = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            //    limit = int.Parse(blocksString);

            //}
            //else if (line.StartsWith("STARS "))
            //{
            //    string blocksString = line.Replace("STARS", string.Empty).Trim();
            //    string[] blocksNumbers = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            //    star1 = int.Parse(blocksNumbers[0]);
            //    star2 = int.Parse(blocksNumbers[1]);
            //    star3 = int.Parse(blocksNumbers[2]);
            //}
             if (line.StartsWith("CAMERA "))
            {
                string blocksString = line.Replace("CAMERA", string.Empty).Trim();
                string[] blocksNumbers = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                                    
                CameraX = float.Parse(blocksNumbers[0]);
                CameraY = float.Parse(blocksNumbers[1]);
                CameraZ = float.Parse(blocksNumbers[2]);
                ProjectionSize = float.Parse(blocksNumbers[3]);

            }
            else if (line.StartsWith("POWERUPS "))
            {
                string blocksString = line.Replace("POWERUPS", string.Empty).Trim();
                string[] blocksNumbers = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < 4; i++)
                {
                    powerups[i] = int.Parse(blocksNumbers[i]);
                }
            }
            
            else
            { //Maps
              //Split lines again to get map numbers
                string[] st = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < st.Length; i++)
                {
                    levelSquares[mapLine, i] = int.Parse(st[i].ToString());
                }
                mapLine++;
            }
        }
    }
    void SaveLevel()
    {
        if (!fileName.Contains(".txt"))
            fileName += ".txt";
        SaveMap(fileName);
    }

    public void SaveMap(string fileName)
    {
        string saveString = "";
        //saveString += "SIZE " + maxCols + "/" + maxRows;
        //saveString += "\r\n";
        // saveString += "LIMIT " + limit;
        //  saveString += "\r\n";
        //  saveString += "STARS " + star1 + "/" + star2 + "/" + star3;
        //saveString += "\r\n";
        //saveString += "POWERUPS " + powerups[0] + "/" + powerups[1] + "/" + powerups[2] + "/" + powerups[3];
        //  saveString += "\r\n";
        saveString += "CAMERA  " + CameraX + "/" + CameraY + "/" + CameraZ + "/" + ProjectionSize;
        saveString += "\r\n";
        //set map data
        for (int row = 0; row < maxRows; row++)
        {
            for (int col = 0; col < maxCols; col++)
            {
                saveString += (int)levelSquares[row, col];
                //if this column not yet end of row, add space between them
                if (col < (maxCols - 1))
                    saveString += " ";
            }
            //if this row is not yet end of row, add new line symbol between rows
            if (row < (maxRows - 1))
                saveString += "\r\n";
        }
        if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
        {
            //Write to file
            string activeDir = Application.dataPath + @"/Resources/Levels/";
            string newPath = System.IO.Path.Combine(activeDir, levelNumber + ".txt");
            StreamWriter sw = new StreamWriter(newPath);
            sw.Write(saveString);
            sw.Close();
        }
        AssetDatabase.Refresh();
    }

    void GUILimit()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(4);
        if (limit <= 0)
            limit = 1;
        GUILayout.Label("Level Ball Limit:", EditorStyles.boldLabel, new GUILayoutOption[] { GUILayout.Width(105) });
        limit = EditorGUILayout.IntField(limit, new GUILayoutOption[] { GUILayout.Width(50) });
        GUILayout.EndHorizontal();
    }
    void GUIStars()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(0);
        GUILayout.BeginVertical();

        GUILayout.Label("Stars:", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        GUILayout.Space(30);
        GUILayout.Label("Star1", new GUILayoutOption[] { GUILayout.Width(100) });
        GUILayout.Label("Star2", new GUILayoutOption[] { GUILayout.Width(100) });
        GUILayout.Label("Star3", new GUILayoutOption[] { GUILayout.Width(100) });
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(30);
        int s = 0;
        s = EditorGUILayout.IntField("", star1, new GUILayoutOption[] { GUILayout.Width(100) });
        if (s != star1)
        {
            star1 = s;
        }
        if (star1 < 0)
            star1 = 10;
        s = EditorGUILayout.IntField("", star2, new GUILayoutOption[] { GUILayout.Width(100) });
        if (s != star2)
        {
            star2 = s;
        }
        if (star2 < star1)
            star2 = star1 + 10;
        s = EditorGUILayout.IntField("", star3, new GUILayoutOption[] { GUILayout.Width(100) });
        if (s != star3)
        {
            star3 = s;
        }
        if (star3 < star2)
            star3 = star2 + 10;
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }
    void GUICameraPosition()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(0);
        GUILayout.BeginVertical();

        GUILayout.Label("Metrix:", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        GUILayout.Space(30);
        GUILayout.Label("Camera-X", new GUILayoutOption[] { GUILayout.Width(70) });       
        CameraX = EditorGUILayout.FloatField(CameraX, new GUILayoutOption[] { GUILayout.Width(50) });
        GUILayout.Label("Camera-Y", new GUILayoutOption[] { GUILayout.Width(70) });
        CameraY = EditorGUILayout.FloatField(CameraY, new GUILayoutOption[] { GUILayout.Width(50) });
        GUILayout.Label("Camera-Z", new GUILayoutOption[] { GUILayout.Width(70) });
        CameraZ = EditorGUILayout.FloatField(CameraZ, new GUILayoutOption[] { GUILayout.Width(50) });               
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Space(30);
        GUILayout.Label("Projection-Size", new GUILayoutOption[] { GUILayout.Width(100) });
        ProjectionSize = EditorGUILayout.FloatField(ProjectionSize, new GUILayoutOption[] { GUILayout.Width(50) });
        GUILayout.EndHorizontal();
    }

    void GUIBlocks()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(0);
        GUILayout.BeginVertical();

        GUILayout.BeginVertical();
        GUILayout.Label("Powerups ON/OFF:", EditorStyles.boldLabel, new GUILayoutOption[] {
            GUILayout.Width (150)
        });

        GUILayout.BeginHorizontal();

        GUILayout.Space(30);
        //-----------Power Button---------------
        GUILayout.BeginHorizontal();
        for (int i = 0; i < lA.AllBallItem.Count; i++)
        {
            if (!lA.AllBallItem[i].BallPower())
                break;
            Texture2D tex = lA.AllBallItem[i].BallInactiveSprite.texture;
            if (powerups[i] == 1)
                tex = lA.AllBallItem[i].BallSprite.texture;
            if (GUILayout.Button(tex, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
            {
                powerups[i] = (int)Mathf.Repeat(powerups[i] + 1, 1.1f);
            }
        }
        //END-----------Power Button---------------
        GUILayout.EndHorizontal();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();


        GUILayout.Label("Items:", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Space(30);
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();

        ShowItemsPanel();

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();

        if (GUILayout.Button(" X ", new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
        {
            selectedBallNo = 0;
        }
        if (GUILayout.Button("Clear \nlevel", new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
        {
            for (int i = 0; i < 70; i++)
            {
                for (int j = 0; j < 11; j++)
                {
                    levelSquares[i, j] = 0;
                }
            }
        }

        GUILayout.Label("Selected Ball:", EditorStyles.boldLabel);
        if (selectedBallNo == 0)
        {
            GUILayout.Button(" X ", new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) });
        }
        else
        {
            Texture2D SelectBallTxr = lA.AllBallItem[selectedBallNo - 1].BallSprite.texture;
            GUILayout.Button(SelectBallTxr, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) });
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

    }
    int selectedBallNo = 0;
    void ShowItemsPanel()
    {
        GUILayout.BeginVertical();
        int BallCounter = 0;
        for (int j = 0; j < lA.AllBallItem.Count / 5 + 1; j++)
        {
            GUILayout.BeginHorizontal();
            for (int i = 0; i < 5; i++)
            {
                if (BallCounter >= lA.AllBallItem.Count)
                    break;
                if (!lA.AllBallItem[BallCounter].BallPower())
                {
                    Texture2D tex = lA.AllBallItem[BallCounter].BallSprite.texture;
                    if (GUILayout.Button(tex, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
                    {
                        selectedBallNo = BallCounter + 1;

                    }
                }
                BallCounter++;
            }

            GUILayout.EndHorizontal();
        }

        GUILayout.EndVertical();
    }
    void GUIGameField()
    {
        GUILayout.Label("Level Desgin:", EditorStyles.boldLabel, new GUILayoutOption[] { GUILayout.Width(100) });
        GUILayout.BeginVertical();
        bool offset = false;

        for (int row = 0; row < maxRows; row++)
        {
            GUILayout.BeginHorizontal();
            if (offset)
            {
                GUILayout.BeginHorizontal();
                //GUILayout.Space(30);

            }

            for (int col = 0; col < maxCols; col++)
            {

                if (levelSquares[row, col] <= 1)
                {
                    var imageButton = new object();
                    if (GUILayout.Button(imageButton as Texture, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
                    {
                        levelSquares[row, col] = selectedBallNo;
                        SaveLevel();
                    }
                }
                else
                {
                    //Debug.Log("Colem no--cv--vf--->" + (levelSquares[row, col] - 1));
                    Texture2D tex = lA.AllBallItem[levelSquares[row, col] - 1].BallSprite.texture;
                    if (GUILayout.Button(tex, new GUILayoutOption[] { GUILayout.Width(50), GUILayout.Height(50) }))
                    {
                        levelSquares[row, col] = selectedBallNo;
                        SaveLevel();
                    }
                }
            }
            GUILayout.EndHorizontal();
            if (offset)
            {
                GUILayout.EndHorizontal();

            }


            offset = !offset;
        }
        GUILayout.EndVertical();
    }

}
