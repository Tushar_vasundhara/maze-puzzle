﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

using UnityEngine.UI;

public class LevelDataView{

    int maxCols;
    int maxRows;
    float CameraX;
    float CameraY;
    float CameraZ;
    float ProjectionSize;
    int limit;
    int star1;
    int star2;
    int star3;
    //int[] powerups;
    int NoOfLineInLevel = 0;
    int[,] levelSquares = new int[70, 11];
  

  

    public LevelDataView(int LevelNo)
    {
        LoadDataFromLocal(LevelNo);
    }
    public int MaxColsofLevel()
    {
        return maxCols;
    }
    public int MaxRowsOfLevel()
    {
        return maxRows;
    }
    public float CamX()
    {
        return CameraX;
    }
    public float CamY()
    {
        return CameraY;
    }
    public float CamZ()
    {
        return CameraZ;
    }
    public float Proj_Size()
    {
        return ProjectionSize;
    }
    public int LimitOfLevel()
    {
        return limit;
    }
    public int Star1OfLevel()
    {
        return star1;
    }
    public int Star2OfLevel()
    {
        return star2;
    }
    public int Satr3OfLevel()
    {
        return star3;
    }
    //public int[] PowersOfLevel()
    //{
    //    return powerups;
    //}
    public int SquareOfLevel(int row , int cols)
    {
        return levelSquares[row, cols];
    }
    public int NoOfLine()
    {
        return NoOfLineInLevel;
    }
    public bool LoadDataFromLocal(int LevelNo)
    {
        //Read data from text file
        TextAsset mapText = Resources.Load("Levels/" + LevelNo) as TextAsset;
      
        if (mapText == null)
        {
            return false;
            mapText = Resources.Load("Levels/" + LevelNo) as TextAsset;
        }
        ProcessGameDataFromString(mapText.text);
        return true;
    }
    bool LineEmty = false;
    void ProcessGameDataFromString(string mapText)
    {
     
        string[] lines = mapText.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
        int mapLine = 0;
      
        foreach (string line in lines)
        {
            //if (line.StartsWith("SIZE "))
            //{
            //    print("SIZE============>");
            //    string blocksString = line.Replace("SIZE ", string.Empty).Trim();
            //    string[] sizes = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            //    maxCols = int.Parse(sizes[0]);
            //    maxRows = int.Parse(sizes[1]);
            //}
            //else if (line.StartsWith("LIMIT "))
            //{
            //    print("LIMIT============>");
            //    string blocksString = line.Replace("LIMIT ", string.Empty).Trim();
            //    //string[] sizes = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            //    limit = int.Parse(blocksString);

            //}
            //else if (line.StartsWith("STARS "))
            //{
            //    print("STARS============>");
            //    string blocksString = line.Replace("STARS", string.Empty).Trim();
            //    string[] blocksNumbers = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            //    star1 = int.Parse(blocksNumbers[0]);
            //    star2 = int.Parse(blocksNumbers[1]);
            //    star3 = int.Parse(blocksNumbers[2]);
            //}
            //else if (line.StartsWith("POWERUPS "))
            //{
            //    string blocksString = line.Replace("POWERUPS", string.Empty).Trim();
            //    string[] blocksNumbers = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            //    for (int i = 0; i < 4; i++)
            //    { CameraZ = int.Parse(blocksNumbers[2]);
            //        powerups[i] = int.Parse(blocksNumbers[i]);
            //    }

            //}

            if (line.StartsWith("CAMERA "))
            {

                string blocksString = line.Replace("CAMERA ", string.Empty).Trim();

                string[] sizes = blocksString.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                CameraX = float.Parse(sizes[0]);

                CameraY = float.Parse(sizes[1]);

                CameraZ = float.Parse(sizes[2]);

                ProjectionSize = float.Parse(sizes[3]);

           
            }
            else

            { //Maps
              //Split lines again to get map numbers
                string[] st = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < st.Length; i++)
                {                   
                        levelSquares[mapLine, i] = int.Parse(st[i].ToString());                                      
                }
                mapLine++;
            }
        }
    }
}
