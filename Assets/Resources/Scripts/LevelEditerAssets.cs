﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditerAssets : MonoBehaviour {

    public static LevelEditerAssets Instents { get; set; }
    public Sprite[] BubbleSprites;
    public Sprite[] PowerSprites;
    public Sprite[] backgrounds;
    public List<BallProperty> AllBallItem = new List<BallProperty>();

    [System.Serializable]
    public class BallProperty
    {
        public Sprite BallSprite;
        public Sprite BallInactiveSprite;
        public char BallType;
        public int BallID;
        public GameObject prifeb;
        public bool BallPower()
        {
            if(BallType=='P')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}