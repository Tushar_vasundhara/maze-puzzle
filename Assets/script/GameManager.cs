﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Analytics;
using GreedyGame.Runtime;


public class GameManager : MonoBehaviour
{

    public Vector2 origin;
    public RaycastHit2D hit;
    public GameObject[] Colliders;
    bool gamePa;
    LevelDataView lv;
    public static GameManager inastnce { get; set; }
    float rotaion;
    public GameObject[] CircleAnimation;

    public Color[] bgColors;
    public Color[] objectColors;
    public bool OneTimeEntry;

    float alphaG;
    float life_loss;
    public Color color = Color.white;
    public bool Gameover;
    public GameObject TextMeshFirst;
    public GameObject TextMeshLast;
    int randomGeneration;
    public GameObject PausePanel, GameCompletePanel;

    public AudioClip Background, ObjectClick, ButtonClick, Winning, StartSound;
    public Sprite Pause;
    public Sprite Play;

    public Sprite soundOn;
    public Sprite soundOff;
    public GameObject SoundObject;

    public Sprite[] BGSprite;

    private void Awake()
    {

             GoogleMobileAdsDemoScript1.Instance.RequestBanner();
    }

    // Use this for initialization
    void Start()
    {
        inastnce = this;
        float duration = 2f;
        life_loss = 1f / duration;
        alphaG = 1f;
        OneTimeEntry = true;
        Gameover = false;

        int l = bgColors.Length;
        randomGeneration = Random.Range(0, l);
       // randomGeneration = 11;
        Camera.main.backgroundColor = bgColors[randomGeneration];

        string LevelNo = PlayerPrefs.GetInt("LevelNo", 0).ToString();
        int Size = LevelNo.Length;
        if (Size == 1)
        {
            string first = LevelNo.Substring(0, 1);
            TextMeshFirst.GetComponent<TextMeshProUGUI>().text = "0";
            TextMeshLast.GetComponent<TextMeshProUGUI>().text = first;
        }
        else
        {
            string first = LevelNo.Substring(0, 1);
            string Last = LevelNo.Substring(1, 1);
            TextMeshFirst.GetComponent<TextMeshProUGUI>().text = first;
            TextMeshLast.GetComponent<TextMeshProUGUI>().text = Last;
        }


        StartCoroutine(RemoveStartingLevelNo());

        if (PlayerPrefs.GetInt("Sound", 0) == 0)
        {
            SoundObject.GetComponent<Image>().sprite = soundOff;
            StopSound();
        }
        else
        {
            SoundObject.GetComponent<Image>().sprite = soundOn;
            Sound(1, 1);
        }

        int BGSpriteCounter = BGSprite.Length;
        if (PlayerPrefs.GetInt("BGSprite", 0) == BGSpriteCounter)
        {
            PlayerPrefs.SetInt("BGSprite", 0);
        }
        GameObject.Find("Space BackGround").GetComponent<SpriteRenderer>().sprite = BGSprite[PlayerPrefs.GetInt("BGSprite", 0)];

        PlayerPrefs.SetInt("BGSprite", PlayerPrefs.GetInt("BGSprite", 0) + 1);
        if (PlayerPrefs.GetInt("BGSprite", 0) == BGSpriteCounter)
        {
            PlayerPrefs.SetInt("BGSprite", 0);
        }
      
        GameObject.Find("Space BackGround1").GetComponent<SpriteRenderer>().sprite = BGSprite[PlayerPrefs.GetInt("BGSprite", 0)];

        // GoogleMobileAdsDemoScript.Instance.RequestBanner();
        // AdViewTest.instance.LoadBanner();

    }

    // Update is called once per frame

    void Update()
    {

        //for (int i = 0; i < GameObject.Find("LevelMatric").transform.childCount; i++)
        //{
        //    for (int j = 0; j < GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).childCount; j++)
        //    {
        //        if (GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(j).GetComponent<ObjectCounter>())
        //        {

        //            if (GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(j).GetComponent<ObjectCounter>().SetNoOfCollider == 1)
        //            {
        //                GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color = GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetComponent<SpriteRenderer>().color;
        //                break;
        //            }
        //            else
        //            {
        //                GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color = GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color;
        //            }
        //        }
        //    }
        //}


        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (!PausePanel.activeSelf)
            {
                PausePanel.SetActive(true);
            }
            else
            {
                PausePanel.SetActive(false);
            }
        }



        if (Input.GetMouseButtonDown(0) && !EventSystem.current.currentSelectedGameObject)
        {
            origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                                          Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            hit = Physics2D.Raycast(origin, Vector2.zero, 0f);
            if (hit)
            {
                if (hit.transform.tag == "Circle")
                {
                    // hit.transform.GetComponent<Animator>().Play("CircleAnimationOut", -1, 0);

                    SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);



                    // GoogleMobileAdsDemoScript1.Instance.removeBannerAd();
                    
                    if (PlayerPrefs.GetInt("ShowINterstrial", 0) % 3 == 0)
                    {
                      
                        //  GoogleMobileAdsDemoScript1.Instance.ShowInterstitial();
                        // InterstitialAdTest.instance.LoadInterstitial();
                        // InterstitialAdTest.instance.ShowInterstitial();
                    }
                    PlayerPrefs.SetInt("ShowINterstrial", PlayerPrefs.GetInt("ShowINterstrial", 0) + 1);
                   

                    Analytics.CustomEvent("LEVEL_CLEAR:", new Dictionary<string, object> {
                        { "levelnumber", PlayerPrefs.GetInt("LevelNo", 0) }                     
                    });
                    //Analytics.CustomEvent("LEVEL_COMPLETE:" + PlayerPrefs.GetInt("LevelNo", 0));
                   
                }
                if (hit.transform.GetComponent<BoxCollider2D>())
                {
                    //hit.transform.GetComponent<Animator>().enabled = true;
                    rotaion = hit.transform.eulerAngles.z;
                    Sound(2, 1);
                    if (rotaion == 0)
                        hit.transform.GetComponent<Animator>().Play("Rotation1", -1, 0);
                    else if (rotaion == 90)
                        hit.transform.GetComponent<Animator>().Play("Rotation2", -1, 0);
                    else if (rotaion == 180)
                        hit.transform.GetComponent<Animator>().Play("Rotation3", -1, 0);
                    else if (rotaion == 270)
                        hit.transform.GetComponent<Animator>().Play("Rotation4", -1, 0);





                }
            }
        }

        if (GameParthy())
        {
            if (OneTimeEntry)
            {

                OneTimeEntry = false;
                Gameover = true;
                int length = CircleAnimation.Length;
                int randomCircle = Random.Range(0, length);
               // GameObject Circle = Instantiate(CircleAnimation[randomCircle], new Vector3(hit.transform.position.x, hit.transform.position.y, 4f), Quaternion.identity);
               // int l = objectColors.Length;
               // int random = Random.Range(0, l);
               // Circle.GetComponent<SpriteRenderer>().color = objectColors[random];
               // Circle.GetComponent<Animator>().Play("CircleAnimation", -1, 0);
				GameObject.Find("LevelMatric").GetComponent<Animator>().enabled = true;
				GameObject.Find ("LevelMatric").GetComponent<Animator> ().Play ("LevelMetricPopOut", -1, 0f);

                GameObject.Find("Space BackGround").GetComponent<Animator>().enabled = true;
                GameObject.Find("Space BackGround").GetComponent<Animator>().Play("BackGroundPopOut", -1, 0f);
                GameObject.Find("Space BackGround1").GetComponent<Animator>().enabled = true;
                GameObject.Find("Space BackGround1").GetComponent<Animator>().Play("BackGroundPopIN", -1, 0f);
                          
               
                Sound(4, 1);
                GameObject level = GameObject.Find("LevelMatric");
                for (int i = 0; i < level.transform.childCount; i++)
                {
                    level.transform.GetChild(i).GetChild(0).GetComponent<BoxCollider2D>().enabled = false;
                }

                level.GetComponent<Animator>().enabled = true;
                if (PlayerPrefs.GetInt("LevelNo", 0) == 50)
                {

                    OpenGameCompletePanel();
                }
                PlayerPrefs.SetInt("LockLevelID" + (PlayerPrefs.GetInt("LevelNo", 0) + 1), 1);

                PlayerPrefs.SetInt("LevelNo", PlayerPrefs.GetInt("LevelNo", 0) + 1);
                StartCoroutine(NextLevelLoad());
              
            }
            // LevelClear.SetActive(true);            
        }
        //if (Input.GetMouseButtonUp(0))
        //{           
        //}       
        if (Gameover)
        {
            alphaG -= Time.deltaTime * life_loss * 2;
            for (int i = 0; i < GameObject.Find("LevelMatric").transform.childCount; i++)
            {
                GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, alphaG);
				GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, alphaG);

            }
            if (alphaG <= 0)
            {
                Gameover = false;
            }
        }
    }

    public void CameraColorPick()
    {

        for (int i = 0; i < GameObject.Find("LevelMatric").transform.childCount; i++)
        {
            //GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetComponent<SpriteRenderer>().color = bgColors[randomGeneration];
			GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetComponent<SpriteRenderer>().color = Color.white;
            GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color = objectColors[randomGeneration];
        }
    }

    public void OpenGameCompletePanel()
    {
        GameCompletePanel.SetActive(true);

    }
    IEnumerator NextLevelLoad()
    {
        yield return new WaitForSeconds(1f);
        print("level no=====>" + PlayerPrefs.GetInt("LevelNo", 0));

        if (PlayerPrefs.GetInt("LevelNo", 0) > 50)
        {
            PlayerPrefs.SetInt("LevelNo", 1);
        }
        else{
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        }
       


        HomeManager.nextLevel = true;
        GameObject.Find("MainObject").transform.GetChild(1).gameObject.SetActive(true);
        GameObject.Find("MainObject").transform.GetChild(0).gameObject.SetActive(false);

         GoogleMobileAdsDemoScript1.Instance.removeBannerAd();

        if (PlayerPrefs.GetInt("ShowINterstrial", 0) % 3 == 0)
        {
              GoogleMobileAdsDemoScript1.Instance.ShowInterstitial();
        }
        PlayerPrefs.SetInt("ShowINterstrial", PlayerPrefs.GetInt("ShowINterstrial", 0) + 1);

        Analytics.CustomEvent("LEVEL_CLEAR:", new Dictionary<string, object> {
                        { "levelnumber", PlayerPrefs.GetInt("LevelNo", 0) }
                    });       
    }

    bool GameParthy()
    {
        gamePa = false;
        for (int i = 0; i < GameObject.Find("LevelMatric").transform.childCount; i++)
        {
            for (int j = 0; j < GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).childCount; j++)
            {
                if (GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(j).GetComponent<ObjectCounter>())
                {
                    if (GameObject.Find("LevelMatric").transform.GetChild(i).GetChild(0).GetChild(j).GetComponent<ObjectCounter>().SetNoOfCollider == 1)
                    {
                        gamePa = true;
                    }
                    else
                    {
                        gamePa = false;
                        return gamePa;
                    }
                }
            }
        }
        return gamePa;
    }

    IEnumerator RemoveStartingLevelNo()
    {
        yield return new WaitForSeconds(2f);
        Sound(5, 1);
        TextMeshFirst.GetComponent<Animator>().Play("NumerAnimationOut", -1, 0);
        TextMeshLast.GetComponent<Animator>().Play("NumerAnimation1Out", -1, 0);
        LevelMatric.instance.callFromGameManager();
        CameraColorPick();
        print("GameObject.Find().GetComponent<Animator>().enabled = true;");
        GameObject.Find("LevelMatric").GetComponent<Animator>().enabled = true;
        GameObject.Find("LevelMatric").GetComponent<Animator>().Play("LevelMetricPopIN");
    }


    public void Sound(int id, float vol)
    {
        if (PlayerPrefs.GetInt("Sound", 0) == 1)
        {
            switch (id)
            {
                case 1:
                    {
                        GetComponent<AudioSource>().PlayOneShot(Background, 0.7f);
                        StartCoroutine(playSoundreturn());
                        break;
                    }
                case 2: GetComponent<AudioSource>().PlayOneShot(ObjectClick, vol); break;
                case 3:

                    GetComponent<AudioSource>().PlayOneShot(ButtonClick, vol); break;
                case 4:

                    GetComponent<AudioSource>().PlayOneShot(Winning, vol);
                    break;
                case 5:

                    GetComponent<AudioSource>().PlayOneShot(StartSound, vol);
                    break;
            }
        }
    }

    IEnumerator playSoundreturn()
    {
        yield return new WaitForSeconds(Background.length);
        Sound(1, 1);
    }
    public void StopSound()
    {
        GetComponent<AudioSource>().Stop();
    }

    // canvas button clicks

    public void Retry(string name)
    {
        Sound(3, 1);
        PlayerPrefs.SetInt("LevelNo", PlayerPrefs.GetInt("LevelNo", 0));
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        HomeManager.nextLevel = false;
    }
    public void next(string name)
    {
        PlayerPrefs.SetInt("LevelNo", PlayerPrefs.GetInt("LevelNo", 0) + 1);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        HomeManager.nextLevel = false;
    }
    public void Switchscene(string name)
    {
        Sound(3, 1);
        GoogleMobileAdsDemoScript1.Instance.removeBannerAd();
        if (name == "PlayScene")
        {
            print("===================>" + PlayerPrefs.GetInt("LevelNo", 0));          
            Analytics.CustomEvent("LEVEL_FAIL:", new Dictionary<string, object> {
                        { "levelnumber", PlayerPrefs.GetInt("LevelNo", 0) }
                    });
            print("===================>" + PlayerPrefs.GetInt("LevelNo", 0));
        }
        SceneManager.LoadSceneAsync(name);
        HomeManager.nextLevel = false;

    }

    public void setFalse(GameObject obj)
    {
        obj.SetActive(false);
    }
    public void setobject(GameObject obj)
    {
        Sound(3, 1);
        obj.SetActive(true);
    }
    public void Sound()
    {
        // Soundmanager.instance.Button_Clickbutton();
        if (SoundObject.GetComponent<Image>().sprite == soundOn)
        {
            PlayerPrefs.SetInt("Sound", 0);
            SoundObject.GetComponent<Image>().sprite = soundOff;
            StopSound();
            // Soundmanager.instance.BackgroundStop();
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 10);
            SoundObject.GetComponent<Image>().sprite = soundOn;
            Sound(1, 1);
            //  Soundmanager.instance.BackgroundStart();
        }
    }
    public void ChangeIcon(Image img)
    {
        if (img.sprite == Pause)
        {
            img.sprite = Play;
        }
        else
        {
            img.sprite = Pause;
        }

    }
    private void OnDestroy()
    {
        Analytics.FlushEvents();

    }
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            Analytics.FlushEvents();                   
        }
    }
    public void MousedownRefresh()
    {
        GreedyGameAgent.Instance.startEventRefresh();
    }
}
