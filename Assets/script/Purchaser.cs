﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.XR;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Purchaser : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
                                                                //public Product product;
                                                                //public Product product1;
                                                                //public Product product2;
                                                                //public Product product3;
                                                                //public Product product4;

    public static Product removeAds;
      public static Product unloackAll;
    public GameObject alertmessage;
    // public Text coinText;



    public int[] P1Coins;
    //		public int help;
    //		public Map map;
    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.
    //public static string kProductIDConsumable = "com.coin4000";
    //public static string kProductIDConsumable1 = "com.coin8000";
    //public static string kProductIDConsumable2 = "com.coin12000";
    //public static string kProductIDConsumable3 = "com.coin16000";
    //public static string kProductIDConsumable4 = "com.coin25000";
    public static string RemoveAdnonConsumable = "com.puzzle.removeadvertisement";
    public static string UnlockAllnonConsumable = "com.puzzle.unloackall_levels";
    //		T_Money tMoney;


    void Start()
    {

        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
            BuyProductID();
        }
       

        //			tMoney = GameObject.FindObjectOfType<T_Money> ();
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        //builder.AddProduct(kProductIDConsumable, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable1, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable2, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable3, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable4, ProductType.Consumable);
        builder.AddProduct(RemoveAdnonConsumable, ProductType.NonConsumable);
        builder.AddProduct(UnlockAllnonConsumable, ProductType.NonConsumable);

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    //public void BuyConsumable()
    //{
    ////StartCoroutine(CheckConnection(product));
    //print("dmfjvdmkkm------------" + product);
    //m_StoreController.InitiatePurchase(product);

    //}

    //public void BuyConsumable1()
    //{
    //    //StartCoroutine(CheckConnection(product1));
    //    m_StoreController.InitiatePurchase(product1);
    //}

    //public void BuyConsumable2()
    //{
    //    //StartCoroutine(CheckConnection(product2));
    //    m_StoreController.InitiatePurchase(product2);
    //}

    //public void BuyConsumable3()
    //{
    //    //StartCoroutine(CheckConnection(product3));
    //    m_StoreController.InitiatePurchase(product3);
    //}

    //public void BuyConsumable4()
    //{
    //    //StartCoroutine(CheckConnection(product));
    //    m_StoreController.InitiatePurchase(product4);
    //}

    //public void BuyNonConsumable()
    //{
    //    HomeManager.instance.Sound(1,1);
    //    print("ave ceh ahiya======>");
    //    m_StoreController.InitiatePurchase(removeAds);
       
    //}

    public void BuyNonConsumable()
    {
        HomeManager.instance.Sound(1, 1);
        print("ave ceh ahiya======>");
        StartCoroutine(CheckConnection(removeAds));
        //  m_StoreController.InitiatePurchase(removeAds);
    }

    IEnumerator CheckConnection(Product product)
    {
        WWW www = new WWW("https://www.google.com");
        yield return www;
        if (www.error == null)
        {
            m_StoreController.InitiatePurchase(product);
        }
        else
        {
            alertmessage.gameObject.SetActive(true);
            alertmessage.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "No Internet Connection";
        }

    }


        public void BuyNonConsumable2()
        {
            HomeManager.instance.Sound(1, 1);
            print("ave ceh ahiya======>");
            StartCoroutine(CheckConnection(unloackAll));
            //  m_StoreController.InitiatePurchase(removeAds);
        }

    void BuyProductID()
    {
        print("dmfjvdmkkm------------" + IsInitialized());
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // system's products collection.

            //product = m_StoreController.products.WithID(kProductIDConsumable);
            //product1 = m_StoreController.products.WithID(kProductIDConsumable1);
            //product2 = m_StoreController.products.WithID(kProductIDConsumable2);
            //product3 = m_StoreController.products.WithID(kProductIDConsumable3);
            //product4 = m_StoreController.products.WithID(kProductIDConsumable4);
            removeAds = m_StoreController.products.WithID(RemoveAdnonConsumable);
             unloackAll = m_StoreController.products.WithID(UnlockAllnonConsumable);

            //    if (product != null && product.availableToPurchase)
            //    {
            //        PlayerPrefs.SetString("price1", product.metadata.localizedPriceString);
            //    print("product.metadata.localizedPriceString--> " + product.metadata.localizedPriceString);
            //    }
            //    if (product1 != null && product1.availableToPurchase)
            //    {
            //        PlayerPrefs.SetString("price2", product1.metadata.localizedPriceString);
            //    print("product1.metadata.localizedPriceString--> " + product1.metadata.localizedPriceString);
            //}
            //    if (product2 != null && product2.availableToPurchase)
            //    {
            //        PlayerPrefs.SetString("price3", product2.metadata.localizedPriceString);
            //    print("product2.metadata.localizedPriceString--> " + product2.metadata.localizedPriceString);
            //}
            //    if (product3 != null && product3.availableToPurchase)
            //    {
            //        PlayerPrefs.SetString("price4", product3.metadata.localizedPriceString);
            //    print("product3.metadata.localizedPriceString--> " + product3.metadata.localizedPriceString);
            //}
            //    if (product4 != null && product4.availableToPurchase)
            //    {
            //        PlayerPrefs.SetString("price5", product4.metadata.localizedPriceString);
            //    print("product4.metadata.localizedPriceString--> " + product4.metadata.localizedPriceString);
            //}
            if (removeAds != null && removeAds.availableToPurchase)
            {
                PlayerPrefs.SetString("price6", removeAds.metadata.localizedPriceString);
                print("removeAds.metadata.localizedPriceString--> " + removeAds.metadata.localizedPriceString);
            }
            if (unloackAll != null && unloackAll.availableToPurchase)
            {
                PlayerPrefs.SetString("price7", unloackAll.metadata.localizedPriceString);
                print("unloackAll.metadata.localizedPriceString--> " + unloackAll.metadata.localizedPriceString);
            }
            // Otherwise ...
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    //  
    // --- IStoreListener
    //

    //IEnumerator CheckConnection(Product product)
    //{
    //    WWW www = new WWW("https://www.google.com");
    //    yield return www;
    //    if (www.error == null)
    //    {
    //        m_StoreController.InitiatePurchase(product);
    //    }
    //    else {

    //    }
    //}
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        //			Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        BuyProductID();
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {

        // A consumable product has been purchased by this user.
        //if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
        //{
        //    print("nbjhvsdhvb---->");
        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins") + P1Coins[0]);
        //}
        //else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable1, StringComparison.Ordinal))
        //{

        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins") + P1Coins[1]);
        //}
        //else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable2, StringComparison.Ordinal))
        //{

        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins") + P1Coins[2]);
        //}
        //else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable3, StringComparison.Ordinal))
        //{

        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins") + P1Coins[3]);
        //}
        //else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable4, StringComparison.Ordinal))
        //{

        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    PlayerPrefs.SetInt("TotalCoins", PlayerPrefs.GetInt("TotalCoins") + P1Coins[4]);
        //}
        if (String.Equals(args.purchasedProduct.definition.id, RemoveAdnonConsumable, StringComparison.Ordinal))
        {
            print("MSBVC");
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

            // ads 1 etle purchase kari nakhyu
            PlayerPrefs.SetInt("ads", 1);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, UnlockAllnonConsumable, StringComparison.Ordinal))
        {

            print("basguhgah---->nsjkldgf");
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

            PlayerPrefs.SetInt("unlock_all_level", 1);

            for (int a = 0; a < 50; a++)
            {
                PlayerPrefs.SetInt("LockLevelID" + (a + 1), 1);
                levelSelect.instance.Start();
            }

            // for (int i = 1; i <= 20; i++)
            // {

            //    PlayerPrefs.SetInt("LockLevelID" + i, 0);
            //int d = i + 1;
            //  print("purchase LockLevelID" + PlayerPrefs.GetInt("LockLevelID" + i));

            //    LevelSelect.instance.Locks[i - 1].SetActive(false);
            //}

            //PlayerPrefs.SetInt("unlock_all_level", 1);

            //    //for (int i = 1; i <= 20; i++)
            //    //{

            //    //    PlayerPrefs.SetInt("LockLevelID" + i, 0);
            //    //    //int d = i + 1;
            //    //    print("purchase LockLevelID" + PlayerPrefs.GetInt("LockLevelID" + i));
            //    //}
        }

        //coinText.text = PlayerPrefs.GetInt("TotalCoins").ToString();


        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}


