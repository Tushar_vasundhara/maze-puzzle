﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

	// Use this for initialization
	public Image image;
	public Sprite MiTab;
	void Start () {
        //if (DeviceDiagonalSizeInInches () > 7.5f) {
        //	image.GetComponent<Image> ().sprite = MiTab;
        //}
        StartCoroutine(LoadSceneMode());
		
	}
    IEnumerator LoadSceneMode()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadSceneAsync("Home");
    }

}
