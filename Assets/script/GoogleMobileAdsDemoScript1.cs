using System;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using System.Net;
using System.IO;
using System.Collections;



using UnityEngine.SocialPlatforms;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

// Example script showing how to invoke the Google Mobile Ads Unity plugin.
public class GoogleMobileAdsDemoScript1 : MonoBehaviour
{
    private BannerView bannerView;
    public InterstitialAd interstitial;
    public RewardBasedVideoAd rewardBasedVideo;
    public GameObject OuterObject;
    public string namescene;
    public bool internetON = true;
    public bool TimerON = true;
    public bool interstitial_flag = true;
    public bool RequestRewardedFlag = false;


    public static GoogleMobileAdsDemoScript1 Instance { get; set; }

    //**********Leaderboard code

    public Text coinTXT;
    bool flag = true;

    public string videoFor;
 

    public void Awake()
    {

        if (Instance != null)
        {
            Destroy(gameObject);
        }
        //MainCanvas = GameObject.Find ("Canvas");
        //LoadingbarDialog = MainCanvas.transform.GetChild(6).gameObject;
        //LoadingbarDialog.SetActive (false);
        //OuterObject = MainCanvas.transform.GetChild(1).GetChild(2).gameObject;

        if (GameObject.Find("Loading"))
            GameObject.Find("Loading").transform.GetChild(0).gameObject.SetActive(false);




    }
    GameObject loader;
    public void Start()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);



        // loader.SetActive(false);
        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
        this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
        this.rewardBasedVideo.OnAdFailedToLoad += this.HandleRewardBasedVideoFailedToLoad;
        this.rewardBasedVideo.OnAdOpening += this.HandleRewardBasedVideoOpened;
        this.rewardBasedVideo.OnAdStarted += this.HandleRewardBasedVideoStarted;
        this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
        this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;
        this.rewardBasedVideo.OnAdLeavingApplication += this.HandleRewardBasedVideoLeftApplication;

        RequestInterstitial();
       // if (!this.rewardBasedVideo.IsLoaded())
        //{
       //     RequestRewardBasedVideo();
       // }
    }

    // Returns an ad request with custom ad targeting.
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)
             .AddTestDevice("E19949FB5E7C5A28C30A875934AC8181") //SWIPE
                .AddTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916") //Lenovo
                .AddTestDevice("64A3A22A05D9DCDBEC68395FF5048CD1")  //Coolpad
                .AddTestDevice("51A49E7B1B359D1999E5C85CE4F54978") //XOLO
                .AddTestDevice("F9EBC1840023CB004A83005514278635") //MI 6otu (No SIM)
                .AddTestDevice("2442EC754FEF046014B26ACCEEAE9C23") //Micromax New
                .AddTestDevice("413FAED40213710754F4D30AC4F60355")  //INTEX
                .AddTestDevice("A7A19E06342F7D3868ABA7863D707BD7") //Samsung Tab
                .AddTestDevice("78E289C0CB209B06541CB844A1744650") //LAVA
                .AddTestDevice("0E45853B0874EAA6418891AD964CC470") //X-ZIOX
                .AddTestDevice("3C8E4AA9C3802D60B83603426D16E430") //Celkon
                .AddTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE") //Karbonn
                .AddTestDevice("74527FD0DD7B0489CFB68BAED192733D") //Nexus TAB
                .AddTestDevice("BB5542D48765B65F516CF440C3545896") //Samsung j2
                .AddTestDevice("E56855A0C493CEF11A7098FE6EA840CB") //Videocon
                .AddTestDevice("390FED1AE343E9FF9D644C4085C3868E") //jivi
                .AddTestDevice("060e75b488ab06ecc7868321aaa21868") // viedocon
                .AddTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53") //MI Tab
                .AddTestDevice("863D8BAE88E209F38FF3C94A0403C776") //Samsung old
                .AddTestDevice("CC24A5104B97346624E2DD6D5E90EA37") //Samsung new
                .AddTestDevice("517048997101BE4535828AC2360582C2") //motorola

                .AddTestDevice("8BB4BCB27396AB8ED222B7F902E13420") //micromax old
                .AddTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0") //Gionee
                .AddTestDevice("F7803FE72A2748F6028D87DC36D7C574") //Mi Chhotu JIO
                .AddTestDevice("8E9BA0470F19FDC3BE855413AA7455E4") //iVoomi
                .AddTestDevice("BB5542D48765B65F516CF440C3545896") //samusung j2
                .AddTestDevice("DD0A309E21D1F24C324C107BE78C1B88") //Ronak Oreo
                .AddTestDevice("2BD06DE73CDDC01BE31408D5A872B147")  // mi note 4
                          
            .AddKeyword("game")
            .SetGender(Gender.Male)
            .SetBirthday(new DateTime(1985, 1, 1))
            .TagForChildDirectedTreatment(false)
            .AddExtra("color_bg", "9B30FF")
            .Build();
    }

    public void RequestBanner()
    {
        if (PlayerPrefs.GetInt("ads", 0) != 1)
        {

            // These ad units are configured to always serve test ads.
#if UNITY_EDITOR
            string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-4268194143932815/6090375681";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-7238432815417079/2680250544";
#else
		string adUnitId = "unexpected_platform";
#endif

            // Clean up banner ad before creating a new one.
            if (this.bannerView != null)
            {
                this.bannerView.Destroy();
            }

            // Create a 320x50 banner at the top of the screen.
            this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

            // Register for ad events.
            this.bannerView.OnAdLoaded += this.HandleAdLoaded;
            this.bannerView.OnAdFailedToLoad += this.HandleAdFailedToLoad;
            this.bannerView.OnAdOpening += this.HandleAdOpened;
            this.bannerView.OnAdClosed += this.HandleAdClosed;
            this.bannerView.OnAdLeavingApplication += this.HandleAdLeftApplication;

            // Load a banner ad.
            this.bannerView.LoadAd(this.CreateAdRequest());
        }
    }


    public void removeBannerAd()
    {
        if (this.bannerView != null)
        {
            this.bannerView.Destroy();
        }
    }

    public void RequestInterstitial()
    {
     
        // These ad units are configured to always serve test ads.
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-4268194143932815/7567108889";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-7238432815417079/4156983747";
#else
        string adUnitId = "unexpected_platform";
#endif
     
        if (this.interstitial != null)
        {
            this.interstitial.Destroy();
        }
        interstitial_flag = false;

        // Create an interstitial.
        this.interstitial = new InterstitialAd(adUnitId);

        // Register for ad events.
        this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
        this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
        this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
        this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
        this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;

        // Load an interstitial ad.
        this.interstitial.LoadAd(this.CreateAdRequest());
    }

    public void RequestRewardBasedVideo()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        // ca-app-pub-4268194143932815/5032444886
        //ca-app-pub-3940256099942544/5224354917
        string adUnitId = "ca-app-pub-4268194143932815/9043842083";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-7238432815417079/5363315627";
#else
		string adUnitId = "unexpected_platform";
#endif
        RequestRewardedFlag = true;
        //this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), adUnitId);
    }

    public void ShowInterstitial()
    {
        if (this.interstitial.IsLoaded())
        {
            if (PlayerPrefs.GetInt("ads", 0) != 1)
            {
                this.interstitial.Show();
            }
        }
        else
        {
            RequestInterstitial();
            //if (LevelSelect.Instance.gotoLevels) {
            //	LevelSelect.Instance.gotoLevels = false;
            //	if(GameObject.Find ("Loading").transform.GetChild(0).gameObject != null){
            //		GameObject.Find ("Loading").transform.GetChild(0).gameObject.SetActive (true);
            //	}	
            //	LevelSelect.Instance.SelectLevel ();
            //}
            MonoBehaviour.print("Interstitial is not ready yet");
        }
    }




    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
       
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
      
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeftApplication event received");
    }

    #endregion

    #region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLoaded event received");
       
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
      
        MonoBehaviour.print(
            "HandleInterstitialFailedToLoad event received with message: " + args.Message);
        interstitial_flag = true;
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialOpened event received");
       
        Time.timeScale = 0;
        GameManager.inastnce.StopSound();
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        //if (LevelSelect.Instance.gotoLevels) {
        //	LevelSelect.Instance.gotoLevels = false;
        //	GameObject.Find ("Loading").transform.GetChild(0).gameObject.SetActive (true);
        //	LevelSelect.Instance.SelectLevel ();
        //}
        RequestInterstitial();
        Time.timeScale = 1;
        // scoreScript1.Instance.loader.SetActive(true);
        // if (namescene == "Home")
        // {


        // }
        //else
        //{
       
       // SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
       
        // }


        MonoBehaviour.print("HandleInterstitialClosed event received");
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLeftApplication event received");
    }

    #endregion

    #region Native express ad callback handlers

    public void HandleNativeExpressAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleNativeExpressAdAdLoaded event received");
    }

    public void HandleNativeExpresseAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleNativeExpressAdFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleNativeExpressAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleNativeExpressAdAdOpened event received");
    }

    public void HandleNativeExpressAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleNativeExpressAdAdClosed event received");
    }

    public void HandleNativeExpressAdLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleNativeExpressAdAdLeftApplication event received");
    }

    #endregion

    #region RewardBasedVideo callback handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        print("HandleRewardBasedVideoLoaded--------------");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
        //		RequestRewardBasedVideo ();
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        Time.timeScale = 0;
    
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        Time.timeScale = 1;
        if (PlayerPrefs.GetInt("watchVideoSuccess", 0) == 0)
        {
         
        }

        //PlayerPrefs.SetInt("OnlyOnceTimeCall", 0);
        RequestRewardBasedVideo();

        RequestRewardedFlag = false;

    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);
        PlayerPrefs.SetInt("watchVideoSuccess", 1);

       
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    #endregion

    public IEnumerator Switchscene_1(string name)
    {
        GameObject.Find("Canvas").transform.GetChild(5).gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.GetChild(6).gameObject.SetActive(false);

        yield return new WaitForSeconds(0.5f);

        if (GoogleMobileAdsDemoScript1.Instance.interstitial.IsLoaded() && PlayerPrefs.GetInt("ads", 0) != 1)
        {
            print("iiiiiiiiiiiiiiiiiffffffffffffffff--------------------->");
            GoogleMobileAdsDemoScript1.Instance.ShowInterstitial();
        }
        else
        {

            SceneManager.LoadSceneAsync(name);
        }
       
        Time.timeScale = 0;
    }

    public bool checkInternetConnection1()
    {

        string HtmlText = GetHtmlFromUri("http://google.com");
        if (HtmlText == "")
        {
            return false;
        }
        else if (!HtmlText.Contains("schema.org/WebPage"))
        {
            return false;
        }
        else
        {
            return true;//success
        }
    }



    //int count = 0;
    //public IEnumerator ShowRewardVideo()
    //{
    //    yield return rewardBasedVideo.IsLoaded();
    //    GameObject loader = GameObject.Find("Canvas").transform.GetChild(7).gameObject;
    //    count++;
    //    if (count <= 15)
    //    {
    //        if (this.rewardBasedVideo.IsLoaded())
    //        {
    //             GameObject loader = GameObject.Find("Canvas").transform.GetChild(7).gameObject;
    //            this.rewardBasedVideo.Show();
    //        }
    //        else
    //        {
    //            RequestRewardBasedVideo();
    //            StartCoroutine(ShowRewardVideo());
    //        }
    //    }
    //    else
    //    {
    //        count = 0;
    //        GameObject.Find("Canvas").transform.GetChild(3).gameObject.SetActive(true);
    //        loader.SetActive(false);
    //    }
    //}

    public IEnumerator ShowRewardBasedVideo()
    {
        GameObject loader = GameObject.Find("Canvas").transform.GetChild(7).gameObject;
        WWW www = new WWW("https://www.google.com");
        yield return www;
        if (www.error == null)
        {
            if (this.rewardBasedVideo.IsLoaded())
            {
                loader.SetActive(false);
                this.rewardBasedVideo.Show();
            }
            else
            {
                RequestRewardBasedVideo();
                StartCoroutine(WaitForVideo2());
            }
        }
        else
        {
            loader.SetActive(false);
            //GarageSceneManager.Instance.OuterPanel1.SetActive(true);
            // GarageSceneManager.Instance.No_Connection_Panel.SetActive(true);
            // GameObject.Find("Canvas").transform.GetChild(3).gameObject.SetActive(true);
            GameObject.Find("Canvas").transform.GetChild(4).gameObject.SetActive(true);
        }
    }


    IEnumerator WaitForVideo2()
    {
        int a = 0;
        GameObject loader = GameObject.Find("Canvas").transform.GetChild(7).gameObject;
        print("Loader Is Active...................................................");
        while (!this.rewardBasedVideo.IsLoaded() && loader.activeSelf)
        {
            WWW www = new WWW("https://www.google.com");
            if (www.error != null)
            {
                internetON = false;
                break;
            }
            yield return new WaitForSeconds(1f);
            if (a >= 6)
            {
                TimerON = false;
                break;
            }
            a += 1;
            print("a ======================= " + a);
        }
        if (this.rewardBasedVideo.IsLoaded() && loader.activeSelf)
        {
            loader.SetActive(false);
            this.rewardBasedVideo.Show();
        }
        else if (loader.activeSelf)
        {
            loader.SetActive(false);
            if (!internetON)
            {
                // GarageSceneManager.Instance.OuterPanel1.SetActive(true);
                //GarageSceneManager.Instance.No_Connection_Panel.SetActive(true);
                GameObject.Find("Canvas").transform.GetChild(4).gameObject.SetActive(true);
            }
            else
            {
                GameObject.Find("Canvas").transform.GetChild(3).gameObject.SetActive(true);
            }
        }
    }



    public string GetHtmlFromUri(string resource)
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        char[] cs = new char[80];
                        reader.Read(cs, 0, cs.Length);
                        foreach (char ch in cs)
                        {
                            html += ch;
                        }
                    }
                }
            }
        }
        catch
        {
            return "";
        }
        return html;
    }


    public IEnumerator checkInternetConnection2()
    {
        WWW www = new WWW("http://www.google.com");// http://google.com
        loader = GameObject.Find("Canvas").transform.GetChild(5).gameObject;

        print("leaderboard111111111111111------------------------");
        loader.gameObject.SetActive(true);
        yield return www;
        if (www.error == null)
        {
            print("sigin already111111111------------------------------------------******************  " + Social.localUser.authenticated);
            //if(i == 1){
            if (Social.localUser.authenticated)
            {
                print("sigin already------------------------------------------******************");
                ShowLeaderboardUI();

            }
            else
            {
                SIgnIn();
            }
        }
        else
        {
            loader.gameObject.SetActive(false);
            GameObject.Find("Canvas").transform.GetChild(3).gameObject.SetActive(true);
        }
    }


    //***************leaderboard and achiement 

    public void SIgnIn()
    {
        //loader = GameObject.Find("Canvas").transform.GetChild(5).gameObject;
        // loader.gameObject.SetActive(true);
        //coinTXT = GameObject.Find("CoinTXT").GetComponent<Text>();
        // Social.localUser.Authenticate(Success => {
        //if (Success)
        //{
        //	Debug.Log("Authentication successful");
        //             if (PlayerPrefs.GetInt("FirstLaunch", 1) == 1)
        //             {
        //                 lb.LoadScores(RWResult =>
        //                 {
        //                     if (RWResult)
        //                     {
        //                         int m = (int)lb.localUserScore.value;

        //                         PlayerPrefs.SetInt("YourbestDistance", m);

        //                         //coinTXT.text = PlayerPrefs.GetInt("TotalCoins").ToString();      
        //                     }
        //                     else
        //                     {
        //                         Debug.Log("Error retrieving leaderboard");
        //                     }
        //                     //print("TotalCoin------------------------------"+PlayerPrefs.GetInt("TotalCoins"));
        //                 });

        //                 PlayerPrefs.SetInt("FirstLaunch", 0);
        //             }

        //             //coinTXT.text = PlayerPrefs.GetInt("YourbestDistance").ToString();
        //             // AddScoreToLeaderboard(GPGSIds.leaderboard_car_racing_on_impossible_tracks, PlayerPrefs.GetInt("TotalCoins"));
        //             //if (authFlag1)
        //             //{
        //             print("sigin------------------------------------------******************"+ loader.name);
        //             loader.gameObject.SetActive(false);
        //		ShowLeaderboardUI();
        //	//}
        //	//if (authFlag2)
        //	//{
        //	//	authFlag2 = false;
        // //                LoadingbarDialog.gameObject.SetActive(false);
        //	//	ShowAchievementUI();
        //	//}

        //	//if (PlayerPrefs.GetInt("TotalCoins") >= 5000 * PlayerPrefs.GetInt("Achievement500000", 1))
        //	//{
        //	//	PlayerPrefs.SetInt("Achievement500000", PlayerPrefs.GetInt("Achievement500000", 1) + 1);
        //	//	//UnlockAchievement(GPGSIds.achievement_500000_coins);
        //	//}
        //	//if (PlayerPrefs.GetInt("TotalCoins") >= 10000 * PlayerPrefs.GetInt("Achievement1000000", 1))
        //	//{
        //	//	PlayerPrefs.SetInt("Achievement1000000", PlayerPrefs.GetInt("Achievement1000000", 1) + 1);
        //	//	//UnlockAchievement(GPGSIds.achievement_500000_coins);
        //	//}
        //}
        //else
        //{
        //    loader.gameObject.SetActive(false);
        //	Debug.Log("Authentication failed");
        //}
        //});
    }

    public void OpenLEaderboard()
    {

        print("leaderboard------------------------");
        StartCoroutine(checkInternetConnection2());

    }

    public void OpenAchievement()
    {
        if (PlayerPrefs.GetInt("ads", 1) == 1)
        {
            if (this.interstitial.IsLoaded())
            {
                if (PlayerPrefs.GetInt("ads", 0) != 1)
                {
                    this.interstitial.Show();
                }
            }
            else
                RequestInterstitial();
        }

        //MainCanvas = GameObject.Find("Canvas");
        //LoadingbarDialog = MainCanvas.transform.GetChild(6).gameObject;
        //LoadingbarDialog.SetActive(false);
        //OuterObject = MainCanvas.transform.GetChild(1).GetChild(2).gameObject;
        //OuterObject.SetActive(false);
        //StartCoroutine(checkInternetConnection2(2));
        //LoadingbarDialog.gameObject.SetActive(false);
    }

    #region Achievements
    public static void UnlockAchievement(string id)
    {
        Social.ReportProgress(id, 100, Success =>
        {
        });
    }

    public static void IncrementAchievement(string id, int stepToIncrement)
    {
        //PlayGamesPlatform.Instance.IncrementAchievement(id, stepToIncrement, Success => {
        //});
    }

    public static void ShowAchievementUI()
    {
        Social.ShowAchievementsUI();
    }
    #endregion /Achievements

    #region Leaderboards
    public static void AddScoreToLeaderboard(string leaderboardId, int score)
    {
        Social.ReportScore(score, leaderboardId, Success =>
        {
            if (Success)
            {
                print("Success-------------------------------" + score);
            }
            else
            {
                print("Success--fail-------------------------" + score);
            }
        });
    }

    public static void ShowLeaderboardUI()
    {
        //((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(GPGSIds.leaderboard_Arrow_Apple);
        GameObject.Find("Canvas").transform.GetChild(5).gameObject.SetActive(false);
    }
    #endregion /Leaderboards



    //void OnApplicationPause(bool hasFocus)
    //{
    //    if (!hasFocus)
    //    {
    //        print("UnpAuse............................................................................................................................");

    //        StartCoroutine(checkInternetConnection_For_Ads());
    //    }
    //}
    //IEnumerator checkInternetConnection_For_Ads()
    //{
    //    WWW www = new WWW("http://www.google.com");// http://google.com
    //    yield return www;
    //    if (www == null)
    //    {
    //        print("load======" + this.rewardBasedVideo.IsLoaded());
    //        print("RequestRewardedFlag======" + RequestRewardedFlag);

    //        if (!this.rewardBasedVideo.IsLoaded() && !RequestRewardedFlag)
    //        {
    //            print("rewardvideo==========-----------");
    //            RequestRewardBasedVideo();
    //        }
    //        print("interstitial======" + this.interstitial.IsLoaded());
    //        print("RequestRewardedFlag======" + interstitial_flag);
    //        if (!this.interstitial.IsLoaded() && interstitial_flag && PlayerPrefs.GetInt("Ads", 0) != 1)
    //        {
    //            print("interstitial==========-----------------");
    //            RequestInterstitial();
    //        }
    //    }
    //    else
    //    {
    //        print("no internet connections-------------->>>>>>>");
    //    }

    //}
}
