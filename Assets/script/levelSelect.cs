﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class levelSelect : MonoBehaviour
{
    //public GameObject[] LockLevel;
    // Use this for initialization
    public static levelSelect instance { get; set; }
   public void Start()
    {
        instance = this;
        for (int a = 0; a <50; a++)
        {

            //print("LockLevelID---->" + (a + 1) + "--------->" + PlayerPrefs.GetInt("LockLevelID" + (a + 1), 0));
            if (PlayerPrefs.GetInt("LockLevelID" + (a+1), 0) == 1)
            {
                transform.GetChild(a).GetChild(1).gameObject.SetActive(true);
                transform.GetChild(a).GetChild(1).GetComponent<Text>().text = (a + 1).ToString();
                transform.GetChild(a).GetChild(0).gameObject.SetActive(false);             
            }
            else
            {
                transform.GetChild(a).GetChild(1).gameObject.SetActive(false);
                transform.GetChild(a).GetChild(0).gameObject.SetActive(true);
            }           
        }
    }
}
