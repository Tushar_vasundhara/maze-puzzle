﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class HomeManager : MonoBehaviour {

    public GameObject Setting;
    public GameObject Exitpanel;
    public Sprite soundOn;
    public Sprite soundOff;
    public GameObject SoundObject;
    int i, currentindex;
    public Image adImage;
    public GameObject LoadingPanel;
    public GameObject StartingPanel;
    public GameObject LoadingPanelHome;
    public GameObject LevelsPanel,alertmsg;
    public AudioClip Background, ButtonClick;
    public Color[] bgColors;
    int randomGeneration;
    public GameObject Level_Settting,HomePanel;
    public static bool nextLevel = false;
    Vector2 emoj_po;
    GameObject emoj;
    int emoj_index;
    public GameObject Thnku, Main_emoji;

    public List<Sprite> emoji_list;
    public static HomeManager instance { get; set; }

    private static void HandleNotificationOpened(OSNotificationOpenedResult result)
    {
    }

    // Use this for initialization
    void Start() {

        print("device id===-=-=---=---=->>>>>" + SystemInfo.deviceUniqueIdentifier);
        instance = this;


        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        PlayerPrefs.SetInt("LockLevelID" + 1, 1);
        if (PlayerPrefs.GetInt("firstTime", 0) == 0)
        {
            PlayerPrefs.SetInt("Sound", 1);
            PlayerPrefs.SetInt("firstTime", 1);
        }
        //FindObjectOfType<levelSelect>().Start();
        if (PlayerPrefs.GetInt("Sound", 0) == 0)
        {
            StopSound();
            SoundObject.GetComponent<Image>().sprite = soundOff;
        }
        else
        {
            Sound(2, 1);
            SoundObject.GetComponent<Image>().sprite = soundOn;
        }
        int l = bgColors.Length;
        randomGeneration = Random.Range(0, l);
        Camera.main.backgroundColor = bgColors[randomGeneration];

        //a9ba0d0f-64c7-407a-b4fa-ce20cdff61cc
        OneSignal.StartInit("9b11864d-d5f2-4c5c-bd27-1b1f82e15d89")
          .HandleNotificationOpened(HandleNotificationOpened)
          .EndInit();

       
        int Screenwidth = Screen.width;
        int Screenheight = Screen.height;

        float Ratio = (float)Screenwidth / Screenheight;

        if (Ratio > 2)
        {
           
        }
        else
        {
          
        }
        // }

        if (PlayerPrefs.GetString("From", "Splash").Equals("Splash"))
        {
            print("splash ma jay che-----------        show video add============>>");
            GoogleMobileAdsDemoScript1.Instance.ShowInterstitial();
        }
            PlayerPrefs.SetString("From", "Home");   
        if(Level_Settting.activeSelf)
        {
            Level_Settting.GetComponent<Animator>().enabled = false;
        }

        //if (PlayerPrefs.GetString("From", "Splash").Equals("Splash") && PlayerPrefs.GetInt("totalcount") > 0)
        //{
        FindObjectOfType<levelSelect>().Start();

        if (nextLevel)
        {
            GameObject.Find("MainObject").transform.GetChild(1).gameObject.SetActive(true);
            GameObject.Find("MainObject").transform.GetChild(0).gameObject.SetActive(false);
        }


        ReadJson.Instance.ShowOfflineAds(adImage);
        //  }
        // AdViewTest.instance.OnDestroy();
       // GoogleMobileAdsDemoScript1.Instance.removeBannerAd();

    }
  
    // Update is called once per frame
    void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
            
            //   Soundmanager.instance.Button_Clickbutton();
           
            if (LoadingPanel.activeSelf)
             {

             }
             else if(LoadingPanelHome.activeSelf)
             {

             }
            else if(alertmsg.activeSelf)
            {
                alertmsg.SetActive(false);
            }
            else if(HomePanel.GetComponent<RectTransform>().anchoredPosition.x == 0 && !Exitpanel.activeSelf)
            {
                print("1");
                Exitpanel.SetActive(true);
            }
            else if(Exitpanel.activeSelf)
            {
                print("2");
                Exitpanel.SetActive(false);
            }
           else if (adImage.gameObject.GetComponent<RectTransform>().anchoredPosition.x == 0 && adImage.gameObject.activeSelf)
            {
                print("3");
                adImage.gameObject.SetActive(false);
                //    if (PlayerPrefs.GetInt("Sound", 0) == 1)
                //    {
                //        Sound(2, 1);
                //        //Soundmanager.instance.BackgroundStart();
                //    }                               
            }
            else if (LevelsPanel.GetComponent<RectTransform>().anchoredPosition.x == 0 || Setting.GetComponent<RectTransform>().anchoredPosition.x == 0)
             {
                print("4");
                Closesetting_Level();
             }
            // else if (alertmsg.activeSelf)
            // {
            //     alertmsg.SetActive(false);
            // }
            // else if(LevelsPanel.activeSelf)
            // {
            //     LevelsPanel.SetActive(false);               
            // }

            //else if (Setting.activeSelf)
            // {              
            //     closeSettingPanel();
            // }
            // else if (Exitpanel.activeSelf)
            // {
            //     Exitpanel.SetActive(false);
            // }
            // else if (!Setting.activeSelf)
            // {
            //     Exitpanel.SetActive(true);
            // }

        }
	}

    public void Switchscene(string name){
       
        SceneManager.LoadSceneAsync(name);
        LoadingPanelHome.SetActive(true);
        PlayerPrefs.SetInt("Game_modes", 0);
    }

  
    public void Sound()
    {
        Sound(1, 1);
        if (SoundObject.GetComponent<Image>().sprite == soundOn)
        {
            PlayerPrefs.SetInt("Sound", 0);
            SoundObject.GetComponent<Image>().sprite = soundOff;
            StopSound();
           
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 1);
            SoundObject.GetComponent<Image>().sprite = soundOn;
            Sound(2, 1);
           
        }
    }

    public void ClickSettingButton()
    {
        // Soundmanager.instance.Button_Clickbutton();
        Sound(1, 1);
        Setting.gameObject.SetActive(true);
        Setting.GetComponent<Animator>().enabled = true;
        Setting.GetComponent<Animator>().Play("PanelOpenAnimation");
    }

    public void closeSettingPanel()
    {
      //  Soundmanager.instance.Button_Clickbutton();
        Setting.GetComponent<Animator>().Play("PanelCloseAnimation");
        StartCoroutine(closeSettingPanelienumrator());
    }
    IEnumerator closeSettingPanelienumrator()
    {
        yield return new WaitForSeconds(0.7f);
        Setting.gameObject.SetActive(false);
    }
    public void YesButton()
    {
        // Soundmanager.instance.Button_Clickbutton();
        Sound(1, 1);
        Application.Quit();
    }
    public void NoButton(GameObject offpanel)
    {
       // Soundmanager.instance.Button_Clickbutton();
        offpanel.SetActive(false);
    }
    public void MoreGames()
    {
        
         if (PlayerPrefs.GetInt("status", 0) == 1)
         {
        Sound(1, 1);
            StartCoroutine(openInterstial());
        }
    }

    public IEnumerator openInterstial()
    {
        yield return new WaitForSeconds(0.5f);
        Application.OpenURL("https://play.google.com/store/apps/developer?id=Vasundhara+Game+Studios");
    }
    public void Rateapp()
    {
            Sound(1, 1);
            StartCoroutine(OpenRateapp());
       
    }
    public IEnumerator OpenRateapp()
    {
        yield return new WaitForSeconds(0.5f);
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
    }

    public void setFalse(GameObject obj)
    {
        obj.SetActive(false);
    }
    public void setobject(GameObject obj)
    {
        // Soundmanager.instance.Button_Clickbutton();
        Sound(1, 1);
        obj.SetActive(true);
    }

  //  public void ClickonDownloadButton()
   // {
   //     adImage.gameObject.SetActive(false);
   //     Application.OpenURL(PlayerPrefs.GetString("Applink" + currentindex, "0"));
   // }
  

    


    public void selectLevel(int level_no)
    {
        PlayerPrefs.SetInt("watchVideoSuccess", 0);
        // Soundmanager.instance.Button_Clickbutton();


       
          if (PlayerPrefs.GetInt("LockLevelID" + (level_no), 0) == 1)
         {
             Sound(1, 1);
            PlayerPrefs.SetInt("Game_modes", 1);
            PlayerPrefs.SetString("From", "Home");
            LoadingPanelHome.SetActive(true);
            PlayerPrefs.SetInt("LevelNo", level_no);
            //SceneManager.LoadSceneAsync("PlayScene");
           // StartCoroutine(LoadALevel(2));
            GameObject.Find("MainObject").transform.GetChild(1).gameObject.SetActive(true);
            GameObject.Find("MainObject").transform.GetChild(0).gameObject.SetActive(false);
            //Application.LoadLevelAsync(2);
        }
        else
        {       
           Sound(1, 1);    
            alertmsg.SetActive(true); 
            alertmsg.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "COMPLETE  PREVIOUS  LEVEL";
        }
    }

    private IEnumerator LoadALevel(int levelName)
    {
        AsyncOperation async = Application.LoadLevelAsync(levelName);
        while (!async.isDone)
        {
            //          float progres = Mathf.Clamp01 (async.progress / 0.9f);
         //   LoadingText.text = (async.progress * 100).ToString("0") + "%";//(progres * 100).ToString("0") + "%"; //Async progress returns always 0 here  
            yield return async.isDone;
        }
    }
    public void PlayButton()
    {
        PlayerPrefs.SetInt("watchVideoSuccess", 0);
        Sound(1, 1);
        PlayerPrefs.SetInt("Game_modes", 1);
        PlayerPrefs.SetString("From", "Home");
        LoadingPanelHome.SetActive(true);
        PlayerPrefs.SetInt("LevelNo", PlayerPrefs.GetInt("LevelNo", 1));
       // SceneManager.LoadSceneAsync("PlayScene");
        GameObject.Find("MainObject").transform.GetChild(1).gameObject.SetActive(true);
        GameObject.Find("MainObject").transform.GetChild(0).gameObject.SetActive(false);
      
    }
    public void openleaderboard()
    {
        
    }

    public void Sound(int id, float vol)
    {
        if (PlayerPrefs.GetInt("Sound", 0) == 1)
        {
            switch (id)
            {

                case 1:
                    {
                        GetComponent<AudioSource>().PlayOneShot(ButtonClick, vol);
                        break;
                    }
                case 2:
                    {
                        GetComponent<AudioSource>().PlayOneShot(Background, 0.7f);
                        StartCoroutine(playSoundreturn());
                        break;
                    }

            }

        }
    }
    IEnumerator playSoundreturn()
    {
        print("Background.length---------------->" + Background.length);
        yield return new WaitForSeconds(Background.length);
        print("callllllllllllll=================>");
        Sound(2, 1);
    }
    public void StopSound()
    {
        GetComponent<AudioSource>().Stop();
    }
    public void Play()
    {
        Sound(1, 1);
        HomePanel.GetComponent<Animator>().Play("HomePanelAnimationOut", -1, 0);
        Level_Settting.transform.GetChild(1).gameObject.SetActive(false);
        Level_Settting.transform.GetChild(0).gameObject.SetActive(true);
        Level_Settting.GetComponent<Animator>().enabled = true;
        Level_Settting.GetComponent<Animator>().Play("SettingPanelIN", -1, 0);       
    }

    public void SettingButton()
    {
        Sound(1, 1);
        adImage.gameObject.SetActive(false);
        HomePanel.GetComponent<Animator>().Play("HomePanelAnimationOut", -1, 0);
        Level_Settting.GetComponent<Animator>().enabled = true;
        Level_Settting.transform.GetChild(0).gameObject.SetActive(false);
        Level_Settting.transform.GetChild(1).gameObject.SetActive(true);
        Level_Settting.GetComponent<Animator>().Play("SettingPanelIN", -1, 0);
    }
    public void Closesetting_Level()
    {
        Sound(1, 1);
        HomePanel.GetComponent<Animator>().Play("HomePanelAnimationIN", -1, 0);
        Level_Settting.GetComponent<Animator>().enabled = true;
        Level_Settting.GetComponent<Animator>().Play("SettingPanelOut", -1, 0);
        StartCoroutine(SetVisibleAdImage());
       
    }
    IEnumerator SetVisibleAdImage()
    {
        yield return new WaitForSeconds(0.5f);
        adImage.gameObject.SetActive(true);
        ReadJson.Instance.ShowOfflineAds(adImage);
    }
    public void Rate_emoj(int index)
    {

        Sound(1, 1);
        StopCoroutine("Rate_emoj_1");
        StopCoroutine("emoji_text_anim");

        emoj_index = index;
       
        emoj = Exitpanel.transform.GetChild(0).GetChild(emoj_index).gameObject;
        emoj_po = emoj.GetComponent<RectTransform>().anchoredPosition;

        if (index == 2 || index == 3 || index == 4)
        {
            Thnku.SetActive(false);
            Thnku.SetActive(true);
        }

        if (!Main_emoji.activeSelf)
        {
            Main_emoji.SetActive(true);
            Main_emoji.GetComponent<Image>().sprite = emoji_list[(emoj_index - 2)];
            Main_emoji.GetComponent<RectTransform>().anchoredPosition = emoj_po;
            if (emoj_index == 5 || emoj_index == 6)
            {
                // StartCoroutine(Rate_App_1());
            }
        }
        else
        {
            Main_emoji.GetComponent<Image>().sprite = emoji_list[(emoj_index - 2)];
            StartCoroutine("Rate_emoj_1");
        }

        for (int i = 2; i < 7; i++)
        {
            Exitpanel.transform.GetChild(0).GetChild(i).GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -32);
            Exitpanel.transform.GetChild(0).GetChild(i).GetChild(0).GetComponent<Text>().color = new Color32(178, 178, 178, 255);
            Exitpanel.transform.GetChild(0).GetChild(i).GetChild(0).GetComponent<Text>().fontSize = 13;
        }
        StartCoroutine("emoji_text_anim");
        emoj.transform.GetChild(0).GetComponent<Text>().fontSize = 14;
        emoj.transform.GetChild(0).GetComponent<Text>().color = new Color32(255, 255, 255, 255);
    }

    IEnumerator Rate_emoj_1()
    {

        while (Main_emoji.GetComponent<RectTransform>().anchoredPosition.x != emoj_po.x)
        {
            yield return new WaitForSeconds(0.01f);
            Main_emoji.GetComponent<RectTransform>().anchoredPosition = Vector2.MoveTowards(Main_emoji.GetComponent<RectTransform>().anchoredPosition, emoj_po, 30f);
        }
        Main_emoji.GetComponent<RectTransform>().anchoredPosition = emoj_po;

        if (emoj_index == 5 || emoj_index == 6)
        {
            //StartCoroutine(Rate_App_1());
        }
    }
    IEnumerator emoji_text_anim()
    {
        while (emoj.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition.y > -40)
        {
            yield return new WaitForSeconds(0.01f);
            emoj.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, emoj.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition.y - 2f);
        }
    }
    public void ReDirectToApkLink()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
    }
    public void ClickonDownloadButton()
    {
        ReadJson.Instance.ClickonDownloadButton();
    }
   
}
