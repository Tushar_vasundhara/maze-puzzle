﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using UnityEngine.UI;

public class ReadJson : MonoBehaviour {

	int i,currentindex;
	string url;
    Image adImage;
    public GameObject moreAppContent;
    public GameObject MoreAppButton;
    GameObject Content;
    public static ReadJson Instance;
    ParseJsonData PJD = new ParseJsonData();
	public class ParseJsonData
	{
		public string Status;
		public List<string> full_thumb_image = new List<string>();
        public List<Sprite> FullImage = new List<Sprite>();
        public List<string> icon_image = new List<string>();
    }

    private void Awake()
    {
  
        if (Instance != null)
			Destroy(gameObject);
        else
            Instance = this;
      
    }

    // Use this for initialization
    void Start () {
        PlayerPrefs.SetString("From", "Splash");
       
        DontDestroyOnLoad(gameObject);
        url = "http://vasundharaapps.com/artwork_apps/api/AdvertiseNewApplications/15/" + Application.identifier;
       // url = "http://vasundharaapps.com/artwork_apps/api/AdvertiseNewApplications/15/com.limo.simulator.car.driving.impossible.trackscom.car.limo";
        i = PlayerPrefs.GetInt("App_ID", 0);
        StartCoroutine(APIcalling());		
	}
	
    public IEnumerator APIcalling() {
		WWW www = new WWW(url);
		yield return www;

		if (www.error == null)
		{
           
			Processjson(www.text);
		}
	}

	public void Processjson(string JsonString)
	{
        
		JsonData jsonvale = JsonMapper.ToObject(JsonString);
		PJD.Status = jsonvale["status"].ToString();
		int k = 0;
        print("Processjson--------------------------------------"+PJD.Status);
        PlayerPrefs.SetInt("OfflineAdStatus",int.Parse(jsonvale["status"].ToString()));
		if (PJD.Status.Equals("1"))
		{
            PlayerPrefs.SetInt("status", 1);

            for (int i = 0; i < jsonvale["data"].Count; i++)
			{
				if (!jsonvale["data"][i]["full_thumb_image"].ToString().Equals(""))
				{
					PlayerPrefs.SetString("Applink" + k, jsonvale["data"][i]["app_link"].ToString());
					PlayerPrefs.SetString("AppPkgName" + k, jsonvale["data"][i]["package_name"].ToString());
					string newString = jsonvale["data"][i]["full_thumb_image"].ToString();
                
                    newString = newString.Replace(" ", "%20");
                    
                    PJD.full_thumb_image.Add(newString);
                    
                    k++;

                   // string newString1 = jsonvale["data"][i]["full_thumb_image"].ToString();
                   // newString1 = newString1.Replace(" ", "%20");
                  //  PJD.icon_image.Add(newString1);
                  //  print("thumb_image============>>>" + newString1);
                }
			}

            PlayerPrefs.SetInt("custom_more_app_len", PJD.icon_image.Count);
            PlayerPrefs.SetInt("more_applinks_len", jsonvale["more_apps"][0]["sub_category"].Count);

            for (int i = 0; i < jsonvale["more_apps"][0]["sub_category"].Count; i++)
            {
                PlayerPrefs.SetString("MoreAppLink" + i, jsonvale["more_apps"][0]["sub_category"][i]["app_link"].ToString());
            }

            print("jsonvale[].Count--------------------------------------" + jsonvale["data"].Count);
         //   print("Processjson data--------------------------------------"+PJD.full_thumb_image.Count);
            
			StartCoroutine(StoreImage(PJD.full_thumb_image));
           // StartCoroutine(StoreIcon(PJD.icon_image));
        }
	}

    public IEnumerator StoreImage (List<string> Urls) {
        
		for (int i = 0; i < Urls.Count; i++)
		{
            if (!Urls[i].Equals("")) {
                print("IEnumerator StoreImage--------------------------------------" + i + "    " + Urls[i]);
				Texture2D texture = new Texture2D(800, 480);            
				WWW www = new WWW(Urls[i]);           
				yield return www;
				www.LoadImageIntoTexture(texture);
                string path = Application.persistentDataPath + "/"+"texture"+i;
                if (!File.Exists(Application.persistentDataPath + "/" + "texture" + i))
                {                 
                    PlayerPrefs.SetInt("totalcount", PlayerPrefs.GetInt("totalcount") + 1);
                }
				File.WriteAllBytes(path, texture.EncodeToJPG());
				
			}      
		}
        print("totalcount--------------------------------------" + PlayerPrefs.GetInt("totalcount") +"  " + Application.persistentDataPath);
		PlayerPrefs.SetString ("HaveAd", "1");
	}
    public IEnumerator StoreIcon(List<string> Urls)
    {
        for (int i = 0; i < Urls.Count; i++)
        {
            if (Urls[i] != "")
            {

                print("IEnumerator StoreIcon--------------------------------------" + i + "    " + Urls[i]);
                Texture2D texture;
                WWW www = new WWW(Urls[i]);
                yield return www;
                texture = new Texture2D(www.texture.height, www.texture.width);
                www.LoadImageIntoTexture(texture);
                PJD.FullImage.Add(Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0)));
                string path = Application.persistentDataPath + "/" + "icon_texture" + i;
                if (!File.Exists(Application.persistentDataPath + "/" + "icon_texture" + i))                   
                PlayerPrefs.SetInt("totalcount_icon", PlayerPrefs.GetInt("totalcount_icon") + 1);
                File.WriteAllBytes(path, texture.EncodeToPNG());
            }
        }
        print("totalcount--------------------------------------" + PlayerPrefs.GetInt("totalcount_icon") + "  " + Application.persistentDataPath);
        // LoadMoreApps();
    }


    public void ShowOfflineiconPause(Image OfflineadImg)
    {
        adImage = OfflineadImg;

        //  PlayerPrefs.SetInt("App_ID_icon", PlayerPrefs.GetInt("App_ID_icon") + 1);
        
        i = PlayerPrefs.GetInt("App_ID_icon", 0);
        
        Texture2D newTexture = new Texture2D(1, 1);
        print("currntindex===========>" + i);
        currentindex = i;
        //if (PlayerPrefs.GetInt("totalcount", 0) == 0)
        //{
        //    return;
        //}

      
        if (!File.Exists(Application.persistentDataPath + "/" + "icon_texture" + i))
        {

            i = i + 1;          
            if (i == PlayerPrefs.GetInt("totalcount_icon", 0))
            {
                i = 0;
            }
            ShowOfflineiconPause(adImage);
        }
        else
        {
            newTexture.LoadImage(File.ReadAllBytes(Application.persistentDataPath + "/" + "icon_texture" + i));
            Sprite NewSprite = Sprite.Create(newTexture, new Rect(0, 0, newTexture.width, newTexture.height), new Vector2(0, 0));
            adImage.sprite = NewSprite;
            adImage.transform.parent.gameObject.SetActive(true);
            adImage.gameObject.SetActive(true);
            PlayerPrefs.SetInt("App_ID_icon", PlayerPrefs.GetInt("App_ID_icon") + 1);
            if (PlayerPrefs.GetInt("App_ID_icon", 0) >= PlayerPrefs.GetInt("totalcount_icon", 0))
            {
                PlayerPrefs.SetInt("App_ID_icon", 0);
            }
        }

       // adImage.transform.parent.parent.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
        // adImage.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
        //adImage.transform.parent.parent.GetChild(1).GetComponent<Button>().onClick.AddListener(ClickonDownloadButtonPause);
        //adImage.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(clickCloseButton);
    }


    public void ClickonDownloadButton()
    {      
        if (PlayerPrefs.GetInt("totalcount", 0) == 0)
        {
            adImage.gameObject.SetActive(false);
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.highway.traffic.rider.bike.racing3d");
        }
        else
        {
            
            adImage.gameObject.SetActive(false);
              Application.OpenURL(PlayerPrefs.GetString("Applink" + PlayerPrefs.GetInt("App_ID", 0), "0"));
            PlayerPrefs.SetInt("App_ID", PlayerPrefs.GetInt("App_ID"));

            if (PlayerPrefs.GetInt("App_ID", 0) == PlayerPrefs.GetInt("totalcount", 0))
            {
                PlayerPrefs.SetInt("App_ID", 0);
            }
        
        }
    }


    public void ShowOfflineAds(Image OfflineadImg) {
        if (PlayerPrefs.GetInt("totalcount", 0) == 0)
        {
            return;
        }
            adImage = OfflineadImg;

        if (PlayerPrefs.GetInt("App_ID", 0) >= PlayerPrefs.GetInt("totalcount", 0))
        {
            PlayerPrefs.SetInt("App_ID", 0);
        }

        i = PlayerPrefs.GetInt("App_ID",0);

		Texture2D newTexture = new Texture2D(1,1);
      
		if (!File.Exists(Application.persistentDataPath + "/" + "texture" + i)) {
			i = i + 1;
           
			if (i == PlayerPrefs.GetInt("totalcount", 0))
			{
				i = 0;
			}
			ShowOfflineAds (adImage);
		}
		else
		{
            newTexture.LoadImage(File.ReadAllBytes(Application.persistentDataPath + "/" + "texture" + i));       
			Sprite NewSprite = Sprite.Create(newTexture, new Rect(0, 0, newTexture.width, newTexture.height), new Vector2(0, 0));
			adImage.sprite = NewSprite;
			adImage.gameObject.SetActive(true);						
		}

        adImage.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        adImage.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
		adImage.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ClickonDownloadButton);
		adImage.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(clickCloseButton);
	}

  

 //   public void ClickonDownloadButton ()
	//{
	//	adImage.gameObject.SetActive(false);
	//	Application.OpenURL(PlayerPrefs.GetString("Applink"+PlayerPrefs.GetInt("App_ID", 0),"0"));
 //       if (PlayerPrefs.GetInt("Sound", 0) == 1)
 //       {
            

 //       }
 //   }

	public void clickCloseButton(){
		adImage.gameObject.SetActive(false);

		PlayerPrefs.SetInt("App_ID", PlayerPrefs.GetInt("App_ID") + 1);
       
        if (PlayerPrefs.GetInt("App_ID", 0) == PlayerPrefs.GetInt("totalcount", 0))
		{
			PlayerPrefs.SetInt("App_ID", 0);
		}
        
    }

}
