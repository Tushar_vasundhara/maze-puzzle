﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMatric : MonoBehaviour
{

    LevelDataView LevelData;
    public GameObject BallDetailPrefab;
    public Camera camera;
    public GameObject BackGround;
    float xPos = -4.3f;
    float yPos = 4.3f;
    public static LevelMatric instance { get; set; }
    
    // Use this for initialization
    private void Awake()
    {
      
        instance = this;
        //if (PlayerPrefs.GetInt("LevelNo", 0) > 17)
        //{
        //    PlayerPrefs.SetInt("LevelNo", 0);
        //    LevelData = new LevelDataView(PlayerPrefs.GetInt("LevelNo", 0));
        //}
        //else
        //{

        //    LevelData = new LevelDataView(PlayerPrefs.GetInt("LevelNo", 1));
        ////    LevelData = new LevelDataView(17);
        // }
        //float CamX = LevelData.CamX();
        //float CamY = LevelData.CamY();
        //float CamZ = LevelData.CamZ();
        //float PeojectionSize = LevelData.Proj_Size();

        //camera.transform.position = new Vector3(CamX, CamY, -10);
        //BackGround.transform.position = new Vector3(CamX, CamY, 20);
        //camera.GetComponent<Camera>().orthographicSize = PeojectionSize;
        //for (int i = 0; i <= 10; i++)
        //{
        //    for (int j = 0; j < 11; j++)
        //    {
        //        BallProp(i, j);
        //    }
        //}
    }
    public void callFromGameManager()
    {        
        if (PlayerPrefs.GetInt("LevelNo", 0) > 50)
        {
          
            PlayerPrefs.SetInt("LevelNo", 51);
            LevelData = new LevelDataView(PlayerPrefs.GetInt("LevelNo", 0));
        }
        else
        {
           
            LevelData = new LevelDataView(PlayerPrefs.GetInt("LevelNo", 0));          
        }
       
        float CamX = LevelData.CamX();
        float CamY = LevelData.CamY();
        float CamZ = LevelData.CamZ();
        float PeojectionSize = LevelData.Proj_Size();

        camera.transform.position = new Vector3(CamX, CamY, -10);
        BackGround.transform.position = new Vector3(CamX, CamY, 20);

        // get screen retio
        int Screenwidth = Screen.width;
        int Screenheight = Screen.height;

        float Ratio = (float)Screenheight / Screenwidth;

        if (Ratio < 1.6)
        {
            //PeojectionSize = PeojectionSize - 1.9;
            print("ratio------        1.6     ======>" + Ratio);
            if (PeojectionSize == 5)
            {
                camera.GetComponent<Camera>().orthographicSize = PeojectionSize;
             
            }
            else if (PeojectionSize == 6)
            {
                camera.GetComponent<Camera>().orthographicSize = PeojectionSize - 1;
            }
            else if(PeojectionSize == 7)
            {
                camera.GetComponent<Camera>().orthographicSize = PeojectionSize - 1.5f;
            }
            else 
            {
                camera.GetComponent<Camera>().orthographicSize = PeojectionSize - 2;
            }
        }
        else if (Ratio > 1.6 && Ratio < 2)
        {
            print("ratio------ 1.6====2.0 ======>" + Ratio);
            if (PeojectionSize == 5 || PeojectionSize == 6)
            {
                camera.GetComponent<Camera>().orthographicSize = PeojectionSize;
            }
            //else if (PeojectionSize == 6)
            //{
            //    camera.GetComponent<Camera>().orthographicSize = PeojectionSize - 1;
            //}
            else if (PeojectionSize == 7)
            {
                camera.GetComponent<Camera>().orthographicSize = PeojectionSize - 0.5f;
            }
            else
            {
                camera.GetComponent<Camera>().orthographicSize = PeojectionSize - 0.9f;
            }
        }
        else
        {
            print("ratio-------   2.0   ======>" + Ratio);
            camera.GetComponent<Camera>().orthographicSize = PeojectionSize;
        }
        
        for (int i = 0; i <= 10; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                BallProp(i, j);
            }
        }
    }
    void Start()
    {
    }
    void BallProp(int NoRow, int NoCols)
    {
        int ballNo = LevelData.SquareOfLevel(NoRow, NoCols);
        if (ballNo != 0)
        {
            GameObject Ball = new GameObject("Ball");
            Ball.transform.localPosition = new Vector3(xPos, yPos, 0);
            Ball.transform.localScale = new Vector3(0.3f, 0.3f, 0.1f);
            Ball.transform.parent = gameObject.transform;
            GameObject ball1 = Instantiate(BallDetailPrefab.GetComponent<LevelEditerAssets>().AllBallItem[ballNo - 1].prifeb, Ball.transform, Ball.transform);         
            ball1.transform.localPosition = Vector3.zero;
        }
        if (xPos.ToString() == "4.3")
        {
            xPos = -4.3f;
            yPos -= 0.86f;
        }        
        else
        {
            xPos += 0.86f;
        }
        
        //if (ballNo != 0)
        //{
        //   GameObject ball1 =  Instantiate(BallDetailPrefab.GetComponent<LevelEditerAssets>().AllBallItem[ballNo - 1].prifeb, Ball.transform, Ball.transform);
        //    ball1.transform.localPosition = Vector3.zero;            
        //    //Ball.transform.parent = gameObject.transform;
        //    //Ball.AddComponent<BoxCollider2D>().size = new Vector2(5.12f, 5.12f);
        //    //Ball.AddComponent<SpriteRenderer>().sprite = BallDetailPrefab.GetComponent<LevelEditerAssets>().AllBallItem[ballNo - 1].BallSprite;
        //}
    }
    // Update is called once per frame
    void Update()
    {

    }
}